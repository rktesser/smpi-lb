CC = smpicc
CCPLUS= g++
LD = ld

SG_HOME = /home/rktesser/work/doutorado/simgrid-test
SG_SRC = $(SG_HOME)/src
SG_BUILD = $(SG_HOME)/build

CFLAGS = -O2 -march=native -Wall -fPIC -I$(SG_HOME) -I$(SG_SRC)/include -I$(SG_SRC) -I$(SG_BUILD) # -DMIGDBG=1
CPPFLAGS = $(CFLAGS) 
LDFLAGS = -lstdc++ -lsimgrid 

LBS = lb/CentralLB.o lb/GreedyLB.o lb/RefineLB.o
LB_AUX=lb/ckheap.o lb/ckset.o lb/Refiner.o
LBWRAPPERS = lb/LBWrapper.o 
OBJS = tracing.o smpi_lb.o replay.o
PMPI_OBJS = tracing.o pmpi_lb.o

ifdef LOAD_BALANCER
	LB_FLAGS=-DLOAD_BALANCER=$(LOAD_BALANCER)
else
	LB_FLAGS=
endif


all: prebuild replay pmpi_lb

prebuild:
	rm -f smpi_lb.o #Rebuilding this, because the LB may have changed.
	
lb/%.o: lb/%.C lb/%.c lb/%.h
	$(CCPLUS) $(CFLAGS) -c -o $@ $<

%.o: %.c %.h
	$(CC) -x c++ $(CFLAGS) $(LB_FLAGS) -c -o $@ $<

replay: $(LB_AUX) $(LBS) $(LBWRAPPERS) $(OBJS)
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

pmpi_lb: $(PMPI_OBJS)
	$(CC) $(CFLAGS) -shared -Wl,-soname=libpmpilb.so -o libpmpilb.so $^ $(LDFLAGS)

clean:
	rm -f $(LB_AUX) $(LBS) $(LBWRAPPERS) $(OBJS) $(PMPI_OBJS) libpmpilb.so replay



