# SAMPI - Simulated Adaptive MPI#

This repository contains research code for the simulation of load balancing of MPI aplications. 

Our simulator relies on SimGrid to collect and replay traces from a previous execution of an MPI application to test different load balancing parameters (i.e. load balancing heuristics, load balancing frequency, execution platform). The input traces can be obtained through sequential MPI emulation using SimGrid's SMPI interface

## Dependencies ##

This code is dependent on a ***forked version*** of the SimGrid simulation framework, which can be found **[here](https://bitbucket.org/rktesser/simgrid-mpi-lb)**.

As of now, __a new version of this code is being integrated on the official version of SimGrid, as a new SimGrid plugin__. Once this plugin is on par with this separate implementation, we will update this page to reflect it.

The official SimGrid can be found at [http://simgrid.gforge.inria.fr/](http://simgrid.gforge.inria.fr/).

## Contact info: ##

Questions  can be sent to *rktesser at inf dot ufrgs dot br*