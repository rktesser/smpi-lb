#ifndef __LBWRAPPER_H
#define __LBWRAPPER_H

#ifdef __cplusplus
extern "C"{
#endif

  typedef enum LB_names{GREEDY_LB=1, REFINE_LB=2} LB_name;

  typedef struct CentralLB CentralLB;
  typedef struct GreedyLB GreedyLB;
  typedef struct RefineLB RefineLB;
  typedef void LDStats;

  CentralLB *new_LB(LB_name LB);
  
  void LB_work(CentralLB *LB, void *stats);

  void *new_LDStats(CentralLB *LB, int count);

  void LB_setCounters(CentralLB *LB, int n_objs);
  void LB_mapObject(CentralLB *LB, int obj, int proc);
  void LB_mapObjects(CentralLB *LB, int *obj_map);
  void LB_recordLoad(CentralLB *LB, int obj, double load);
  void LB_processMigrations(CentralLB *LB);

  int LB_from_proc(CentralLB *LB, int obj_id);
  int LB_to_proc(CentralLB *LB, int obj_id);

#ifdef __cplusplus
}
#endif


#endif
