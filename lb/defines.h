#ifndef _LB_DEFINES_H_
#define _LB_DEFINES_H_

#ifndef CkPrintf
#define CkPrintf printf
#endif

#ifndef CmiPrintf
#define CmiPrintf printf
#endif


#include<stdio.h>

#ifndef CmiAbort
#include <stdlib.h>
static void __printerr_and_abort_(const char *msg)
{
  fprintf(stderr, msg);
  exit(-1);
}

#define CmiAbort(message) \
  __printerr_and_abort_(message)

#endif

#include <vector>

//For now, we will substitute CkVec for std::vector
#ifndef CkVec
#define CkVec std::vector
#endif

#include "lbdb.h"

//TODO Implement this.
#define CkNumPes() 10


#ifndef CrnRand
#include <time.h>
extern unsigned int __rand_seed; // declared on CentralLB.C
static int __internal_rand()
{
  if(! __rand_seed){
    __rand_seed = (unsigned int) time(NULL);
  }
  return rand_r(&__rand_seed);
}

#define CrnRand __internal_rand
#endif

#endif
