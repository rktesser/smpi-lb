#include <stdbool.h>
#include "smpi/include/private.h"
#include "smpi/include/smpi_coll.hpp"
#include "tracing.h"
#include "simgrid/sg_config.h"

#ifdef __cplusplus
extern "C"{
#endif

double smpilb_host_speed;

/******************** Memory usage tracking **********************************/

unsigned int *smpilb_process_size;

static void init_memory_tracker()
{
  xbt_assert(smpilb_process_size = (unsigned int *) calloc(
	simcall_process_count(), sizeof(unsigned int)));
}

void *_smpilb_malloc(size_t size)
{
  void *ret = malloc(size);
  if(ret){
    smpilb_process_size[smpi_process_index()] += (unsigned int)size;
  }
  return ret;
}

void *_smpilb_calloc(size_t nmemb, size_t size)
{
  void *ret = calloc(nmemb, size);
  if(ret){
    smpilb_process_size[smpi_process_index()] +=
      (unsigned int)nmemb * (unsigned int)size;
  }
  return ret;
}

void *_smpilb_realloc(void *ptr, size_t size)
{
  void *ret = realloc(ptr, size);

  // FIXME There is no standard way to keep track of old size of the allocated
  // memory.
   
  //if(ret) smpilb_memory_usage[smpi_process_index()] += (unsigned int)size;
  
  return ret;
}

void _smpilb_free(void *ptr)
{
  // FIXME There is no standard way to keep track of old size of the allocated
  // memory.*/
  free(ptr);
  return;
}


/*********** Experimental code to trace the duration of iteractions **********/

int print_iteration_push(FILE *trace_file, char *process_id,
    instr_extra_data extra)
{
  if(trace_file && process_id){
    fprintf(trace_file, "%s iteration_in\n", process_id);
  }else{
    return 0;
  }
  return 1;
}

int print_iteration_pop(FILE *trace_file, char *process_id,
    instr_extra_data extra)
{
  if(trace_file && process_id){
    fprintf(trace_file, "%s iteration_out\n", process_id);
  }else{
    return 0;
  }
  return 1;
}

int print_migration_push(FILE *trace_file, char *process_id,
    instr_extra_data extra)
{
  if(trace_file && process_id){
    fprintf(trace_file, "%s migrate %d\n", process_id, extra->send_size);
  }else{
    return 0;
  }
  return 1;
}

int PMPI_Iteration_in(MPI_Comm comm)
{
  smpi_bench_end();
#ifdef HAVE_TRACING
  int rank = comm != MPI_COMM_NULL ? smpi_process_index() : -1;
  instr_extra_data extra = xbt_new0(s_instr_extra_data_t,1);
  extra->type = TRACING_CUSTOM;
  extra->print_push = print_iteration_push;
  extra->print_pop = print_iteration_pop;
  extra->src = rank;

  TRACE_Iteration_in(rank, extra);//implemented on instr_smpi.c
#endif
  smpi_bench_begin();
  return 1;
}

int PMPI_Iteration_out(MPI_Comm comm)
{	
  smpi_bench_end();
#ifdef HAVE_TRACING
  int rank = comm != MPI_COMM_NULL ? smpi_process_index() : -1;
  TRACE_Iteration_out(rank);//implemented on instr_smpi.c
#endif
  smpi_bench_begin();
 return 1;
}

void PMPI_Migrate(void)
{
  instr_extra_data extra = xbt_new0(s_instr_extra_data_t, 1);
  extra->type = TRACING_CUSTOM;
  extra->print_push = print_migration_push;
  extra->print_pop = NULL;
  extra->send_size = smpilb_process_size[smpi_process_index()];
  extra->src = smpi_process_index();
  TRACE_migration_call(smpi_process_index(), extra);
}

int MPI_Iteration_in(MPI_Comm comm)
{
  return PMPI_Iteration_in(comm);
}

int MPI_Iteration_out(MPI_Comm comm)
{	
  return PMPI_Iteration_out(comm);
}

void MPI_Migrate(void)
{
  PMPI_Migrate();
}

int MPI_Init(int *argc, char ***argv)
{
  int ret;
  ret = PMPI_Init(argc, argv);
  if(ret == MPI_SUCCESS){
    if(smpi_process_index() == 0){
      init_memory_tracker();
      smpilb_host_speed = xbt_cfg_get_double("smpi/host-speed");
    }
    simgrid::smpi::Colls::barrier(MPI_COMM_WORLD);
  }
  return ret;
}

/* Function call used to inject computation into an SMPI simulation. */
void smpilb_execute(double seconds)
{
  /* 
   * smpi_execute_flops does not generate computation traces. Therefore, we
   * need to call smpi_execute. The problem is that this function takes an
   * computation time in the host machine (the one that is running the
   * simulation. But the time we have is simulation time (the time the
   * computation lasted in the simulated machine. So, we need to do a
   * conversion from simulated time to host time.
   */

  double speed = sg_host_speed(sg_host_self()); // speed of simulated machine
  double flops_simulated = seconds * speed; 

  /*Now, we divide the flops by the speed of the host, to obtain the duration
   * of the computation in the host. */
  double seconds_host = flops_simulated/smpilb_host_speed;

  smpi_execute(seconds_host);
}

void smpilb_bench_end()
{
  smpi_bench_end();
}

void smpilb_bench_begin()
{
  smpi_bench_begin();
}
#ifdef __cplusplus
}
#endif

