#ifndef _PMPI_LB_H
#define _PMPI_LB_H

#include <unistd.h>
#include <stddef.h>
#include <sys/time.h>
#include <xbt/misc.h>
#include <xbt/function_types.h>

#include "mpi.h"

#define malloc(nbytes)\
  _smpilb_malloc(nbytes)

#define calloc(n_elm,elm_size)\
  _smpilb_calloc(n_elm,elm_size)

#define realloc(ptr,nbytes)\
  _smpilb_realloc(ptr, nbytes)

#define free(ptr)\
  _smpilb_free(ptr);

#ifdef _WIN32
#define MPI_CALL(type,name,args) \
  type name args; \
  type P##name args
#else
#define MPI_CALL(type,name,args) \
  type name args __attribute__((weak)); \
  type P##name args
#endif

#ifdef __cpluplus
extern "C"{
#endif

/***************** Calls to trace iteraction times ***************************/
MPI_CALL(XBT_PUBLIC(int), MPI_Iteration_in, (MPI_Comm comm));
MPI_CALL(XBT_PUBLIC(int), MPI_Iteration_out, (MPI_Comm comm));
/*****************************************************************************/

MPI_CALL(XBT_PUBLIC(void), MPI_Migrate, (void));

void smpilb_execute(double seconds);
void smpilb_bench_end();
void smpilb_bench_begin();

void *_smpilb_malloc(size_t size);
void *_smpilb_calloc(size_t nmemb, size_t size);
void *_smpilb_realloc(void *ptr, size_t size);
void _smpilb_free(void *ptr);

#ifdef __cpluplus
}
#endif

#endif
