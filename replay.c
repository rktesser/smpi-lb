/* Copyright (c) 2009-2014. The SimGrid Team.
 * All rights reserved.                                                     */

/* This program is free software; you can redistribute it and/or modify it
 * under the terms of the license (GNU LGPL) which comes with this package. */

#include <stdio.h>
#include "smpi/smpi.h"
#include "smpi_lb.h"

int main(int argc, char *argv[])
{

  smpi_replay_init(&argc, &argv); 
 
  //This function registers our custom actions.
  smpi_lb_init();

  smpi_replay_main(&argc, &argv); 
  
  return 0;
}
