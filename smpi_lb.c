#include <stdio.h>
#include <math.h>
#include <xbt.h>
#include <xbt/replay.hpp>
#include <simgrid/simix.h>
#include <stdbool.h>
#include "surf/surf.h" //needed by the function hosts_as_dynar.
#include "smpi/mpi.h"
#include "smpi/include/private.h"
#include "smpi/include/smpi_process.hpp"
#include "lb/LBWrapper.h"
#include "smpi_lb.h" 
#include "tracing.h"
#include "simix/smx_synchro_private.hpp"

#ifdef __cplusplus
extern "C"{
#endif

//extern xbt_lib_t host_lib;
static CentralLB *lb = NULL;
static xbt_dynar_t host_list;
static int *exp_pc = NULL;

static xbt_dict_t old_actions;

#ifdef LOAD_BALANCER
static LDStats *statsLD = NULL;
static smx_mutex_t mutex_lb;
static msg_bar_t smpilb_bar;
static unsigned int smpilb_barrier_initialized = 0;
#endif 


/************************** Auxiliary functions*******************************/ 


/* Function that returns a dynar containing all hosts that have processes 
 * assigned to them. */
xbt_dynar_t hosts_as_dynar(void)
{
  unsigned int i;
  unsigned int n_hosts = sg_host_count();
  sg_host_t *hosts = sg_host_list();
  xbt_dynar_t hosts_dynar = xbt_dynar_new(sizeof(sg_host_t), NULL);

  for(i = 0; i < n_hosts; i++){
        if(sg_host_get_process_count(hosts[i]) > 0){
	  xbt_dynar_push(hosts_dynar, &hosts[i]);
#ifdef MIGDBG
	  fprintf(stderr, "Added host: %s\n", sg_host_get_name(hosts[i]));
#endif
	}
  }
  return hosts_dynar;
}

int print_nprocs(const char *fun, const char *file, int line)
{
  sg_host_t host;
  unsigned int cursor;
  int p_count, p_total = 0;
  printf("\n%d: In function \'%s\' (%s:%d).\n", smpi_process()->index(), fun,
      file, line);
  printf("There are %d processes.\n", simcall_process_count());
  xbt_dynar_t hosts = hosts_as_dynar();
  xbt_dynar_foreach(hosts, cursor, host){
    p_count = sg_host_get_process_count(host);
    p_total += p_count;
    printf("Host %u (\"%s\") has %d processes.\n", cursor,
	sg_host_get_name(host), p_count);
  }
  xbt_dynar_free(&hosts);
  printf("%d processes in total.\n", p_total);
  fflush(stdout);
  return p_total;
}

int check_process_counts()
{
  int i,sg_count;
  unsigned long  from, to;
  int ret = 1;
  sg_host_t host;
  unsigned long cursor;
  int n = simcall_process_count();
  unsigned long nh = xbt_dynar_length(host_list);
  int *pcounts = (int *) calloc(nh, sizeof(int));
  
  if(!pcounts) return 0;

  for(i = 0; i < n; i++){
    from = LB_from_proc(lb, i);
    to = LB_to_proc(lb, i);
    if(from < 0 || from >= nh || to < 0 || to >= nh){
      printf("%d: ERROR! Invalid host index. from: \'%ld\'; to: \'%ld\'.\n",
	  smpi_process()->index(), from, to); fflush(stdout);
      free(pcounts);
      return 0;
    }
    pcounts[to]++;
  }

  xbt_dynar_foreach(host_list, cursor, host){
    if(cursor < 0 || cursor >= nh){
      printf("%d: ERROR! Invalid host index. cursor: \'%ld\'.\n",
	  smpi_process()->index(), cursor); fflush(stdout);
      free(pcounts);
      return 0;
    }
    sg_count = sg_host_get_process_count(host);
    if(pcounts[cursor] != sg_count){
      printf("%d: ERROR! Process counts for %lu(%s) do not match: %d != %d\n",
	  smpi_process()->index(), cursor, sg_host_get_name(host),
	  pcounts[cursor], sg_count);
      ret = 0;
    }/*else{
      printf("%d: OK! Process counts for %s match: %d == %d\n",
	  smpi_process()->index(), sg_host_get_name(host),
	  pcounts[cursor], sg_count);
    }*/
  }
  if(ret == 0) printf("\n");
  fflush(stdout);

  free(pcounts);
  return ret;
}

int check_process_counts_v2()
{
  int sg_count;
  unsigned long from, to;
  int ret = 1;
  sg_host_t host;
  unsigned long cursor;
  int n = simcall_process_count();
  unsigned long nh = xbt_dynar_length(host_list);
 
  for(int i = 0; i < n; i++){
    from = LB_from_proc(lb, i);
    to = LB_to_proc(lb, i);
    if(from < 0 || from >= nh || to < 0 || to >= nh){
      printf("%d: ERROR! Invalid host index. from: \'%ld\'; to: \'%ld\'.\n",
	  smpi_process()->index(), from, to); fflush(stdout);
      return 0;
    }
  }
  
  for(unsigned long i = 0; i < nh; i++){
    fprintf(stderr, "Expected process count for %ld: %d\n", i, exp_pc[i]);
    fflush(stderr);
  }
  xbt_dynar_foreach(host_list, cursor, host){
    if(cursor < 0 || cursor >= nh){
      printf("%d: ERROR! Invalid host index. cursor: \'%ld\'.\n",
	  smpi_process()->index(), cursor); fflush(stdout);
      return 0;
    }
    sg_count = sg_host_get_process_count(host);
    if(exp_pc[cursor] != sg_count){
      printf("%d: ERROR! Process counts for %lu (%s) do not match: %d != %d\n",
	  smpi_process()->index(), cursor, sg_host_get_name(host),
	  exp_pc[cursor], sg_count);
      ret = 0;
    }/*else{
      printf("%d: OK! Process counts for %s match: %d == %d\n",
	  smpi_process()->index(), sg_host_get_name(host),
	  exp_pc[cursor], sg_count);
    }*/
  }
  printf("\n");
  fflush(stdout);

  return ret;
}


#ifdef LOAD_BALANCER
/* This function was copied form SimGrid (src/smpi/smpi_replay.c */
static double parse_double(const char *string)
{
  double value;
  char *endptr;
  value = strtod(string, &endptr);
  if (*endptr != '\0')
    THROWF(unknown_error, 0, "%s is not a double", string);
  return value;
}
#endif //LOAD_BALANCER

/********************* End of auxiliary functions ****************************/

/* This function simulates what happens when the original application calls
 * MPI_Migrate. It executes the load balancing heuristics, makes the necessary
 * migrations and updates the task mapping in the load balancer. */

static void action_migrate(const char *const *action)
{
#ifndef LOAD_BALANCER
  return;
#else


  //The only parameter is the ammount of memory used by the current process.
  CHECK_ACTION_PARAMS(action, 1, 0);
  
  unsigned long mem_size = 0; 
  sg_host_t *new_host, host;
  
  int my_id = smpi_process()->index();
  int from_proc, to_proc;
  int bpch, bpcnh, apch, apcnh;

#ifdef MIGDBG
  static int mig_pc = 0;
  static int mig_counter = 0;
  static unsigned long mig_mem = 0;
  static double mig_timer = 0;

  double before, after;
#endif

  if(action[2]){
    sscanf(action[2], "%lu", &mem_size);
  }
#ifdef HAVE_TRACING
  TRACE_migration_call(smpi_process()->index(), NULL);
#endif
  
  TRACE_lb_heuristic_in(my_id);
  smpi_bench_begin();
  MSG_barrier_wait(smpilb_bar);

  if(my_id == 0){
    LB_work(lb, statsLD); // Run the heuristics.
  }
  
  MSG_barrier_wait(smpilb_bar);
  smpi_bench_end();
  TRACE_lb_heuristic_out(my_id);

  // Make the migrations!
  simcall_mutex_lock(mutex_lb);

#ifdef MIGDBG
  if(mig_pc == 0){
    mig_timer = smpi_process()->simulated_elapsed();
  }
#endif

  from_proc = LB_from_proc(lb, my_id);
  to_proc = LB_to_proc(lb, my_id);
  mutex_lb->unlock(SIMIX_process_self());

  if(from_proc != to_proc){
   
    simcall_mutex_lock(mutex_lb);
    new_host = (sg_host_t *) xbt_dynar_get_ptr(host_list, to_proc);
    host = sg_host_self();
    mutex_lb->unlock(SIMIX_process_self());


    //Sending the process data.
#ifdef MIGDBG
    before = smpi_process()->simulated_elapsed();
#endif
    simgrid::smpi::smpi_replay_send_process_data((double) mem_size, *new_host);
#ifdef MIGDBG
    after = smpi_process()->simulated_elapsed();
#endif
    

    //Updating the process and host mapping in SimGrid.
    simcall_mutex_lock(mutex_lb);

#ifdef MIGDBG    
    ++mig_counter;
    mig_mem += mem_size;
    exp_pc[from_proc]--;
    exp_pc[to_proc]++;
#endif

    bpch = sg_host_get_process_count(host);
    bpcnh = sg_host_get_process_count(*new_host);
    simgrid::smpi::smpi_replay_process_migrate(SIMIX_process_self(), *new_host, mem_size);
    apch = sg_host_get_process_count(host);
    apcnh = sg_host_get_process_count(*new_host);
    if((bpch - 1) != apch){
      printf("%d: ERROR! Process count for original host (\"%s\") is not the expected: (%d - 1)!= %d\n", smpi_process()->index(), sg_host_get_name(host), bpch, apch);
    }
    if((bpcnh + 1) != apcnh){
      printf("%d: ERROR! Process count for new host (\"%s\") is not the expected: (%d + 1) != %d\n", smpi_process()->index(), sg_host_get_name(*new_host), bpcnh, apcnh);
    }

#ifdef MIGDBG
    printf("Migrated rank %d form %d(\"%s\") to %d(\"%s\") (%lu bytes) "
       	"in %lf seconds.\n", my_id, from_proc, sg_host_get_name(host), to_proc,
	sg_host_get_name(*new_host), mem_size, (after - before));
    fflush(stdout);
#endif

    mutex_lb->unlock(SIMIX_process_self());
  }

#ifdef MIGDBG
  simcall_mutex_lock(mutex_lb);
  mig_pc ++;
  if(mig_pc == simcall_process_count()){
    mig_timer = smpi_process()->simulated_elapsed() - mig_timer;
    mig_pc = 0;
  }
  mutex_lb->unlock(SIMIX_process_self());
  MSG_barrier_wait(smpilb_bar);

  simcall_mutex_lock(mutex_lb);
  if(!check_process_counts()){
    check_process_counts_v2();
  }
  mutex_lb->unlock(SIMIX_process_self());
#endif

  MSG_barrier_wait(smpilb_bar);
  
  if(my_id == 0){    
    LB_processMigrations(lb); // Update mapping and reset loads.
#ifdef MIGDBG
    printf("%d processes migrated in %lf seconds, totalizing %lu bytes.\n\n",
	mig_counter, mig_timer, mig_mem);
    mig_counter = mig_mem = 0;
#endif
    }
  MSG_barrier_wait(smpilb_bar);

#ifdef MIGDBG
  simcall_mutex_lock(mutex_lb);
  if(!check_process_counts()){
    check_process_counts_v2();
  }
  mutex_lb->unlock(SIMIX_process_self());
  MSG_barrier_wait(smpilb_bar);
#endif


#endif // LOAD_BALANCER
}


/* This function instantiates the load balancer and struct LDStats. It also
 * sets the number of processes and tasks and their initial mapping. */
void init_lb()
{
#ifndef LOAD_BALANCER
  return;
#else

  int t, n_hosts, n_tasks;
  sg_host_t host;
  smx_actor_t process;
  xbt_dynar_t process_list; 
  unsigned int host_cursor, process_cursor;
  int *task_map;

  mutex_lb = simcall_mutex_init();

  //initialize the load balancer.
  host_list = hosts_as_dynar();
  //printf("Initializing the LB.\n"); fflush(stdout); 
  lb = new_LB(LOAD_BALANCER);
  //printf("LB Intialized!\n"); fflush(stdout); 
  n_hosts = xbt_dynar_length(host_list);
  n_tasks = simcall_process_count();
 
  xbt_assert(exp_pc = (int *) calloc(n_hosts, sizeof(int)));

  //printf("Initializing LDStats with %d hosts.\n", n_hosts); fflush(stdout); 
  statsLD = new_LDStats(lb, n_hosts); //This assumes only one process per host.
  //printf("LDStats initialized!\n"); fflush(stdout); 
  
  //printf("Initializing counters with %d tasks.\n", n_tasks);  fflush(stdout);
  LB_setCounters(lb, n_tasks);
  //printf("Counters initialized!\n"); fflush(stdout); 

  //printf("Creating the task map.\n"); fflush(stdout);
  xbt_assert(task_map = (int *) calloc(n_tasks, sizeof(int)));
  xbt_dynar_foreach(host_list, host_cursor, host){
    process_list = sg_host_get_processes_as_dynar(host);
   
#ifdef MIGDBG
    int p_count = xbt_dynar_length(process_list);
    printf("Host %d(\"%s\") has %d tasks.\n", (int) host_cursor,
	sg_host_get_name(host), p_count);
    fflush(stdout);
#endif

    xbt_dynar_foreach(process_list, process_cursor, process){
      t = smpi_rank_of_smx_process(process);
      // This next line should work for now, but, is it future proof?
      task_map[t] = (int) host_cursor;
      exp_pc[host_cursor]++;
    }
    xbt_dynar_free(&process_list);
  }
  
  //printf("Initializing the mapping.\n"); fflush(stdout); 
  LB_mapObjects(lb, task_map);
  //printf("Mapping intialized!\n"); fflush(stdout);
  free(task_map); 

#endif
}


/* This function should override the original action_init form smpi_replay.c */
void smpilb_action_init(const char *const *action)
{
  //printf("\nInitializing a task.\n\n"); fflush(stdout);
  action_fun old_action = (action_fun) xbt_dict_get_or_null(old_actions, "init");
  old_action(action);


#ifndef LOAD_BALANCER
  if(smpi_process()->index() == 0){
    printf("\nRunning without a load balancer.\n"); fflush(stdout);
  }
  return;
#else


  //There must be a better way to do this!
  if(smpi_process()->index() == 0){
    smpilb_bar = MSG_barrier_init(simcall_process_count());
    smpilb_barrier_initialized = 1;
  }else{
    while(! smpilb_barrier_initialized){
      simcall_process_sleep(0.05);
    }
  }
  
  //printf("\n%d: Initialized.\n\n", smpi_process()->index()); fflush(stdout);
  
  MSG_barrier_wait(smpilb_bar);

  if(smpi_process()->index() == 0){
    //printf("\n%d: Initializing the load balancing.\n\n", smpi_process()->index()); fflush(stdout);
    init_lb();
    //printf("\n%d: Load balancing intialized!\n\n", smpi_process()->index()); fflush(stdout);
  }
  
/* TODO Check if this barrier changes the application's behavior. Is there a
 * barrier at the end of MPI_Init? */
  MSG_barrier_wait(smpilb_bar);
#endif //LOAD_BALANCER


}

/* Custom action to override action_compute, allowing us to update the amount
 * of computing in LDStats. */
void smpilb_action_compute(const char *const *action)
{
  CHECK_ACTION_PARAMS(action, 1, 1);

#ifdef LOAD_BALANCER
  double comp;
  
  
  if(!action[3]){//Computation is expressed in flops.
    comp = parse_double(action[2]);
  }else{//Computation is expressed in seconds.
    comp = parse_double(action[3]);
  }
 
  /* smpirun/smpicc registers a computation at the start of the trace (before
   * init). If we try to record the load then, it will give us a SEGFAULT. So,
   * we need to test if the LB has already been istantiated. */
  if(lb && statsLD){
    LB_recordLoad(lb, smpi_process()->index(), comp);
  }
#endif

  action_fun old_action  = (action_fun) xbt_dict_get(old_actions, "compute");
  old_action(action);
}


/******************************************************************************
 *         Code to include the duration of iterations in the trace.           *
 ******************************************************************************/

void action_iteration_in(const char *const *action)
{
  CHECK_ACTION_PARAMS(action, 0, 0);
#ifdef HAVE_TRACING
  TRACE_Iteration_in(smpi_process()->index(), NULL);
#endif
}

void action_iteration_out(const char *const *action)
{
  CHECK_ACTION_PARAMS(action, 0, 0);
#ifdef HAVE_TRACING
  TRACE_Iteration_out(smpi_process()->index());
#endif
}

/*****************************************************************************
 * Finalize
 * ***************************************************************************/

void smpilb_action_finalize(const char *const *action)
{

/* TODO
 * free the host list;
 * destroy the load balancer;
 */
  
  static int count = 0, np = -1;

  if(np < 0){
    np = SIMIX_process_count();
  }

  action_fun old_action = (action_fun)xbt_dict_get(old_actions, "finalize");
  old_action(action);

  if(++count == np){
    xbt_dict_free(&old_actions);
    xbt_dynar_free(&host_list);
#ifdef LOAD_BALANCER 
    free(exp_pc);
#endif
  }
  
}

/*****************************************************************************/

/* This function initializes the actions used in the lb code. */
void smpi_lb_init()
{
  static unsigned int done = 0;
  if(smpi_rank_of_smx_process(SIMIX_process_self()) == 0){
    old_actions = xbt_dict_new_homogeneous(NULL);
    xbt_dict_set(old_actions, "init",
	(void *) xbt_replay_action_get("init"), NULL);
    xbt_dict_set(old_actions, "finalize",
	(void *) xbt_replay_action_get("finalize"), NULL);
    xbt_dict_set(old_actions, "compute",
	(void *) xbt_replay_action_get("compute"), NULL);

    //printf("Registering custom actions!\n"); fflush(stdout);
    xbt_replay_action_register("migrate",	 action_migrate);
    xbt_replay_action_register("iteration_in",	 action_iteration_in);
    xbt_replay_action_register("iteration_out",	 action_iteration_out);
    xbt_replay_action_register("init",	 smpilb_action_init);
    xbt_replay_action_register("compute", smpilb_action_compute);
    xbt_replay_action_register("finalize", smpilb_action_finalize);

    done = 1;

  }else while(!done) sleep(0.1);
}




#ifdef __cplusplus
}
#endif

