#ifndef __SMPI_LB_H
#define __SMPI_LB_H

//Macro copied from SimGrid (src/smpi/smpi_replay.c).
#define CHECK_ACTION_PARAMS(action, mandatory, optional) {\
    int i=0;\
    while(action[i]!=NULL)\
     i++;\
    if(i<mandatory+2)                                           \
    THROWF(arg_error, 0, "%s replay failed.\n" \
          "%d items were given on the line. First two should be process_id and action.  " \
          "This action needs after them %d mandatory arguments, and accepts %d optional ones. \n" \
          "Please contact the Simgrid team if support is needed", __FUNCTION__, i, mandatory, optional);\
}
 
#ifdef __cplusplus
extern "C"{
#endif

void smpi_lb_init();

#ifdef __cplusplus
}
#endif

#endif
