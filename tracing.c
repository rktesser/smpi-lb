#include <stdbool.h>
#include "smpi/include/private.h"
#include "smpi/include/smpi_process.hpp"
#include <simgrid/simix.h>
#include "instr/instr_smpi.h"
#include "tracing.h" 
#include <ctype.h>
/*
 * These static functions have been copied straight from SimGrid
 * (src/smpi/instr_smpi.c). Another solution would be do remove the static
 * qualifier from their declaration.
 */

#ifdef __cplusplus
extern "C"{
#endif

static const char *smpi_colors[] ={
    "migrate",	"0 0.5 0",
    "iteration", "0.5 0 0.5",
    "lb_heuristic", "0.5 0.5 0.5",
    NULL, NULL,
};

static char *str_tolower (const char *str)
{
  char *ret = xbt_strdup (str);
  int i, n = strlen (ret);
  for (i = 0; i < n; i++)
    ret[i] = tolower (str[i]);
  return ret;
}

static const char *instr_find_color (const char *state)
{
  char *target = str_tolower (state);
  const char *ret = NULL;
  const char *current;
  unsigned int i = 0;
  while ((current = smpi_colors[i])){
    if (strcmp (state, current) == 0){ ret = smpi_colors[i+1]; break; } //exact match
    if (strstr(target, current)) { ret = smpi_colors[i+1]; break; }; //as substring
    i+=2;
  }
  free (target);
  return ret;
}


static void cleanup_extra_data (instr_extra_data extra){
  if(extra!=NULL){
    if(extra->sendcounts!=NULL)
      xbt_free(extra->sendcounts);
    if(extra->recvcounts!=NULL)
      xbt_free(extra->recvcounts);
    xbt_free(extra);
  }
}


/*********** Experimental code to trace the duration of iterations ***********/

void TRACE_Iteration_in(int rank, instr_extra_data extra)
{
  if (!TRACE_smpi_is_enabled()) {
      cleanup_extra_data(extra);
      return;
  }
  const char *operation = "iteration";
  char str[INSTR_DEFAULT_STR_SIZE];
  smpi_container(rank, str, INSTR_DEFAULT_STR_SIZE);
  container_t container = PJ_container_get (str);
  type_t type = PJ_type_get ("MPI_STATE", container->type);
  const char *color = instr_find_color (operation);
  value* val = value::get_or_new (operation, color, type);
  if(extra != NULL){
    new PushStateEvent(SIMIX_get_clock(), container, type, val,
      (void *)extra);
  }else{
    new PushStateEvent(SIMIX_get_clock(), container, type, val);
  }
}

void TRACE_Iteration_out(int rank)
{
  if (!TRACE_smpi_is_enabled()) return;

  char str[INSTR_DEFAULT_STR_SIZE];
  smpi_container(rank, str, INSTR_DEFAULT_STR_SIZE);
  container_t container = PJ_container_get (str);
  type_t type = PJ_type_get ("MPI_STATE", container->type);

  new PopStateEvent(SIMIX_get_clock(), container, type);
}


/****************************************************************************
 * Code to trace when action_migrate is executed (MPI_Migrate)              *
 ****************************************************************************/

void TRACE_migration_call(int rank, instr_extra_data extra)
{
  if (!TRACE_smpi_is_enabled()) return;
  
  char str[INSTR_DEFAULT_STR_SIZE];
  smpi_container(rank, str, INSTR_DEFAULT_STR_SIZE);
  container_t container = PJ_container_get (str);
  type_t type = PJ_type_get ("MPI_Migrate", container->type);
  
  const char *operation = "migrate";
  const char *color = instr_find_color (operation);
  if(smpi_process()->replaying()){//When replaying, we register an event.
    char str[INSTR_DEFAULT_STR_SIZE];
    smpi_container(rank, str, INSTR_DEFAULT_STR_SIZE);
    container_t container = PJ_container_get (str);
    type_t type = PJ_type_get ("MPI_Migrate", container->type);
    value* val = value::get_or_new(operation, color, type); 
    new NewEvent(SIMIX_get_clock(), container, type, val);
  }else{
    // Ugly workaround!
    // TI tracing uses states as events, and does not support printing events.
    // So, we need a different code than for replay in order to be able to
    // generate ti_traces for the migration calls.
    if (!TRACE_smpi_is_enabled()) {
      cleanup_extra_data(extra);
      return;
    }
    value* val = value::get_or_new (operation, color, type);
    new PushStateEvent(SIMIX_get_clock(), container, type, val,
	(void *)extra);
    new PopStateEvent(SIMIX_get_clock(), container, type);
  }
}


/*****************************************************************************
 * Code to trace the duration of the LB heuristics                           *
 * ***************************************************************************/
void TRACE_lb_heuristic_in(int rank)
{
  if (!TRACE_smpi_is_enabled())
      return;
  const char *operation = "lb_heuristic";
  char str[INSTR_DEFAULT_STR_SIZE];
  smpi_container(rank, str, INSTR_DEFAULT_STR_SIZE);
  container_t container = PJ_container_get (str);
  type_t type = PJ_type_get ("MIGRATE_STATE", container->type);
  const char *color = instr_find_color (operation);
  value* val = value::get_or_new (operation, color, type);
  new PushStateEvent(SIMIX_get_clock(), container, type, val);
}

void TRACE_lb_heuristic_out(int rank)
{
  if (!TRACE_smpi_is_enabled()) return;

  char str[INSTR_DEFAULT_STR_SIZE];
  smpi_container(rank, str, INSTR_DEFAULT_STR_SIZE);
  container_t container = PJ_container_get (str);
  type_t type = PJ_type_get ("MIGRATE_STATE", container->type);

  new PopStateEvent(SIMIX_get_clock(), container, type);
}




/* TEMPORARY WORKAROUND TO TRACE INJECTED COMPUTATIONS 
 * It would be better to call the tracing functions included in simgrid*/
/*
void TRACE_smpilb_computing_in(int rank, instr_extra_data extra)
{
  //do not forget to set the color first, otherwise this will explode
  if (!TRACE_smpi_is_enabled()|| !TRACE_smpi_is_computing()) {
      cleanup_extra_data(extra);
      return;
  }

  char str[INSTR_DEFAULT_STR_SIZE];
  smpi_container(rank, str, INSTR_DEFAULT_STR_SIZE);
  container_t container = PJ_container_get (str);
  type_t type = PJ_type_get ("MPI_STATE", container->type);
  val_t value = PJ_value_get_or_new ("computing", nullptr, type);
  new_pajePushStateWithExtra  (SIMIX_get_clock(), container, type, value, static_cast<void*>(extra));
}

void TRACE_smpilb_computing_out(int rank)
{
  if (!TRACE_smpi_is_enabled()|| !TRACE_smpi_is_computing())
    return;
  char str[INSTR_DEFAULT_STR_SIZE];
  smpi_container(rank, str, INSTR_DEFAULT_STR_SIZE);
  container_t container = PJ_container_get (str);
  type_t type = PJ_type_get ("MPI_STATE", container->type);
  new_pajePopState (SIMIX_get_clock(), container, type);
}
*/

#ifdef __cplusplus
}
#endif
 
