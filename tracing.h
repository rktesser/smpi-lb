#ifndef _LB_TRACING_H
#define _LB_TRACING_H

#include "instr/instr_private.h"

#ifndef HAVE_TRACING
#define HAVE_TRACING
#endif

#ifdef __cplusplus
extern "C"{
#endif

void TRACE_Iteration_in(int rank, instr_extra_data extra);
void TRACE_Iteration_out(int rank);
void TRACE_migration_call(int rank, instr_extra_data extra);


void TRACE_lb_heuristic_in(int rank);
void TRACE_lb_heuristic_out(int rank);

#ifdef __cplusplus
}
#endif

#endif
